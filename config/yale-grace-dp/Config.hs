{-# LANGUAGE OverloadedStrings #-}

-- To use, create a symlink 'app/Config.hs' pointing to this file

module Config where

import Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))

configDir :: [Char]
configDir = "/home/fas/poland/dp484/project/hyperion-projects/config/yale-grace-dp/"

storageDir :: [Char]
storageDir = "/gpfs/gibbs/pi/poland/dp484/hyperion/"

config :: HyperionBootstrapConfig
config = HyperionBootstrapConfig
  { -- Email address for cluster notifications
    emailAddr = Just "david.poland@yale.edu"

    -- Working directory for data files
  , dataDir = storageDir ++ "data"

    -- Save logs to this directory
  , logDir = storageDir ++ "logs"

    -- Directory for databases used during computation
  , databaseDir = storageDir ++ "db"

   --  Hyperion makes a (temporary) copy of its executable in order to run
  --  remote copies of itself. Store executables in this directory
  , execDir = storageDir ++ "bin"

  --  Directory for SLURM jobs
  , jobDir = storageDir ++ "jobs"

    -- Path to a script that does srun sdpb, after loading
    -- appropriate modules and setting environment variables
  , srunSdpbExecutable = configDir ++ "srun_sdpb.sh"

    -- Path to a script that runs pvm2sdp, after loading
    -- appropriate modules and setting environment variables
    -- , srunPvm2sdpExecutable = configDir ++ "srun_pvm2sdp.sh"

    -- Path to a script that runs sdpb (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdpbExecutable = configDir ++ "sdpb.sh"

    -- Path to a script that runs pvm2sdp (without srun), after loading
    -- appropriate modules and setting environment variables
    -- , pvm2sdpExecutable = configDir ++ "pvm2sdp.sh"

    -- Path to a script that runs sdp2input (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdp2inputExecutable = configDir ++ "sdp2input.sh"

    -- Path to a script that runs sdp2input, after loading
    -- appropriate modules and setting environment variables
  , srunSdp2inputExecutable = configDir ++ "srun_sdp2input.sh"

    -- Path to a script that runs scalar_blocks, after loading appropriate
    -- modules and setting environment variables
  , scalarBlocksExecutable = configDir ++ "scalar_blocks.sh"

    -- Path to a script that runs blocks_3d, after loading appropriate
    -- modules and setting environment variables
  , blocks3dExecutable = configDir ++ "blocks_3d.sh"

  , blocks4dExecutable = ""
  , seeds4dExecutable = ""

    -- Path to the qdelaunay executable (needed for Delaunay triangulation searches)
  , qdelaunayExecutable = "/home/fas/poland/rse23/bin/qdelaunay"

    -- Path to a script that runs tiptop
  , tiptopExecutable = "/home/fas/poland/rse23/bin/tiptop"

    -- Command to use to run the hyperion program. If no option is
    -- specified, hyperion will copy its own executable to a safe place and
    -- run that. Default: Nothing
  , hyperionCommand = Nothing

    -- Default SLURM queue (partition) to submit jobs to. Default: Nothing
  , defaultSlurmPartition = Just "pi_poland,day"

  , defaultSlurmAccount = Nothing
  , sshRunCommand = Nothing
  }
