#!/bin/sh

numactl --interleave=all mongod --dbpath ${HOME}/mongodb/data/db --logpath ${HOME}/mongodb/mongodb.log --bind_ip_all &
