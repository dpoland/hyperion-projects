{-# LANGUAGE OverloadedStrings #-}

-- To use, create a symlink 'app/Config.hs' pointing to this file

module Config where

import Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))

configDir :: [Char]
configDir = "/home/fas/poland/rse23/project/hyp-project/config/yale-grace-rse/"

config :: HyperionBootstrapConfig
config = HyperionBootstrapConfig
  { -- Email address for cluster notifications
    emailAddr = "rajeev.erramilli@yale.edu"

    -- Working directory for data files
  , dataDir = "/home/fas/poland/rse23/scratch60/hyperion/data"

    -- Save logs to this directory
  , logDir = "/home/fas/poland/rse23/project/hyp-project/logs"

    -- Directory for databases used during computation
  , databaseDir = "/home/fas/poland/rse23/project/hyp-project/db"

   --  Hyperion makes a (temporary) copy of its executable in order to run
  --  remote copies of itself. Store executables in this directory
  , execDir = "/home/fas/poland/rse23/project/hyp-project/bin"

  --  Directory for SLURM jobs
  , jobDir = "/home/fas/poland/rse23/project/hyp-project/jobs"

    -- Path to a script that does srun sdpb, after loading
    -- appropriate modules and setting environment variables
  , srunSdpbExecutable = configDir ++ "srun_sdpb.sh"

    -- Path to a script that runs pvm2sdp, after loading
    -- appropriate modules and setting environment variables
  , srunPvm2sdpExecutable = configDir ++ "srun_pvm2sdp.sh"

    -- Path to a script that runs sdpb (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdpbExecutable = configDir ++ "sdpb.sh"

    -- Path to a script that runs pvm2sdp (without srun), after loading
    -- appropriate modules and setting environment variables
  , pvm2sdpExecutable = configDir ++ "pvm2sdp.sh"

    -- Path to a script that runs scalar_blocks, after loading appropriate
    -- modules and setting environment variables
  , scalarBlocksExecutable = configDir ++ "scalar_blocks.sh"

    -- Path to the qdelaunay executable (needed for Delaunay triangulation searches)
  , qdelaunayExecutable = "/home/fas/poland/rse23/bin/qdelaunay"

    -- Command to use to run the hyperion program. If no option is
    -- specified, hyperion will copy its own executable to a safe place and
    -- run that. Default: Nothing
  , hyperionCommand = Nothing

    -- Default SLURM queue (partition) to submit jobs to. Default: Nothing
  , defaultSlurmPartition = Just "pi_poland,day"
  }
