{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.JJJJ4d2020 where

import           Bounds.JJJJ4d                               (JJJJ4d (..))
import qualified Bounds.JJJJ4d                               as JJJJ
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.Reader                        (local)
import           Data.Aeson                                  (ToJSON)
import           Data.Binary                                 (Binary)
import           Data.Text                                   (Text)
import           GHC.Generics                                (Generic)
import           Hyperion                                    hiding (dataDir)
import           Hyperion.Bootstrap.BinarySearch             (BinarySearchConfig (..),
                                                              Bracket (..))
import           Hyperion.Bootstrap.CFTBound                 (CFTBound (..),
                                                              BinarySearchBound(..),
                                                              remoteBinarySearchBoundWithFileTreatment,
                                                              defaultResultBoolClosure,
                                                              CFTBoundFiles (..),
                                                              BoundFileTreatment(..),
                                                              remoteComputeBoundWithFileTreatment,
                                                              keepAllFiles,
                                                              FileTreatment(..),
                                                              defaultCFTBoundFiles)
import           Hyperion.Bootstrap.CFTBound.ParamCollection (spinsNmax, jumpFindingParams)
import           Hyperion.Bootstrap.Main                     (HyperionBootstrapConfig (..),
                                                              UnknownProgram (..))
import           Hyperion.Util                               (minute)
import           Projects.Defaults                           (defaultBoundConfig)
import qualified SDPB.Blocks.Blocks4d                        as B4d
import           Config                                      (config)
import           SDPB.Bounds.Spectrum                        (setGap,
                                                              setTwistGap,
                                                              unitarySpectrum)
import           SDPB.Solver                                 (Params (..))
import qualified SDPB.Solver                                 as SDPB
import           System.FilePath.Posix                       ((</>))

persistentJJJJ4dBlockDir :: B4d.Block4dParams -> FilePath
persistentJJJJ4dBlockDir B4d.Block4dParams {..} = (dataDir config) </> "blocks" </> "JJJJ4d" </>
  ("nmax-"  ++ show nmax ++
  "_kpo-"   ++ show keptPoleOrder ++
  "_kpo_f-" ++ show keptPoleOrder_final ++
  "_order-" ++ show order ++
  "_ff-"    ++ show floatFormat ++
  "_prec-"  ++ show precision)

mkFilesJJJJ4d :: CFTBound Int JJJJ4d -> FilePath -> CFTBoundFiles
mkFilesJJJJ4d cftBound dir = (defaultCFTBoundFiles dir)
  { blockDir = persistentJJJJ4dBlockDir (blockParams (bound cftBound)) }

-- | Importantly, we DO NOT remove the blockDir
treatmentJJJJ4d :: BoundFileTreatment
treatmentJJJJ4d = keepAllFiles
  { sdpDirTreatment  = RemoveFile
  , jsonDirTreatment = RemoveFile
  }

remoteComputeJJJJ4dBound :: CFTBound Int JJJJ4d -> Cluster SDPB.Output
remoteComputeJJJJ4dBound cftBound =
  remoteComputeBoundWithFileTreatment treatmentJJJJ4d (mkFilesJJJJ4d cftBound) cftBound

data JJJJ4dBinarySearch = JJJJ4dBinarySearch
  { ffbs_bound     :: CFTBound Int JJJJ4d
  , ffbs_config    :: BinarySearchConfig Rational
  , ffbs_gapSector :: (Int, Int)
  } deriving (Show, Generic, Binary, ToJSON)

remoteJJJJ4dBinarySearch :: JJJJ4dBinarySearch -> Cluster (Bracket Rational)
remoteJJJJ4dBinarySearch ffbs =
  remoteBinarySearchBoundWithFileTreatment treatmentJJJJ4d (mkFilesJJJJ4d (ffbs_bound ffbs)) $
  MkBinarySearchBound
  { bsBoundClosure      = static mkBound `ptrAp` cPure (ffbs_bound ffbs) `cAp` cPure (ffbs_gapSector ffbs)
  , bsConfig            = ffbs_config ffbs
  , bsResultBoolClosure = defaultResultBoolClosure
  }
  where
    mkBound cftBound gapSector gap =
      cftBound { bound = (bound cftBound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = setGap gapSector gap (spectrum (bound cftBound))

jjjj4dDefaultGaps :: Int -> JJJJ4d
jjjj4dDefaultGaps nmax = JJJJ4d
  { spectrum     = setTwistGap 1e-6 $
                   unitarySpectrum
  , objective    = JJJJ.Feasibility
  , blockParams  = B4d.block4dParamsNmax nmax
  , spins        = spinsNmax nmax
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "JJJJ4ddAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 28) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeJJJJ4dBound . bound)
  [ 1 -- Should be allowed
  , 9 -- Should be disallowed
  ]
  where
    nmax = 6
    bound deltaS = CFTBound
      { bound = (jjjj4dDefaultGaps nmax)
        { spectrum     = setGap (0,0) deltaS $
                         setGap (0,4) 4 $
                         setTwistGap 1 $
                         unitarySpectrum
        --, blockParams = (B4d.block4dParamsNmax nmax) { B4d.keptPoleOrder = 12 }
        }
      , precision = B4d.precision (B4d.block4dParamsNmax nmax :: B4d.Block4dParams)
      , solverParams = (jumpFindingParams nmax) { precision = 1260 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "JJJJ4d_binary_search_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (30*minute)) $
  mapConcurrently_ (remoteJJJJ4dBinarySearch . search)
  [ () ]
  where
    nmax = 6
    search () = JJJJ4dBinarySearch
      { ffbs_bound = CFTBound
        { bound = jjjj4dDefaultGaps nmax
        , precision = B4d.precision (B4d.block4dParamsNmax nmax :: B4d.Block4dParams)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, 0)
      , ffbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 1
          , falsePoint = 10
          }
        , threshold  = 1e-3
        , terminateTime = Nothing
        }
      }

boundsProgram p = throwM (UnknownProgram p)
